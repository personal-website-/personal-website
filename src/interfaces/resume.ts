import type { StrapiBase } from './strapi-base'

export type Skill = StrapiBase & {
  label: string
  color: string
}

export type Network = StrapiBase & {
  id: number
  label: string
  url: string
  icon: {
    url: string
  }
}

export type Resume = StrapiBase & {
  bio: string
  skills: Skill[]
  networks: Network[]
  resume: {
    url: string
  }
}
