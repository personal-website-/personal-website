import type { StrapiBase } from './strapi-base'

export type Link = StrapiBase & {
  name: string
  url: string
}

export type Tag = StrapiBase & {
  name: string
}

export type Project = StrapiBase & {
  slug: string
  title: string
  description: string
  tags: Tag[]
  links: Link[]
  heroImage: any
  date: string
  locale: string
}
