import type { StrapiBase } from './strapi-base'

export type Local = StrapiBase & {
  name: string
  code: string
  isDefault: boolean
}
