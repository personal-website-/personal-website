export const useStrapiMedias = (): ((mediaPath: string) => string) => {
  return (mediaPath: string) =>
    `${import.meta.env.STRAPI_URL}${mediaPath}` as string
}
