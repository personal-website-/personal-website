import * as en from '../i18n/en.json'
import * as fr from '../i18n/fr.json'

const defaultLang = 'en'

const translations = {
  en: { ...en } as any,
  fr: { ...fr } as any,
}

export type Lang = keyof typeof translations

const get = (object: any, path: any) =>
  path.split('.').reduce((acc: any, value: any) => acc[value], object)

export const useTranslations = (lang: Lang) => {
  return (key: string) => {
    return get(translations[lang], key) || get(translations[defaultLang], key)
  }
}
