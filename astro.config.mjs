// @ts-check
import { defineConfig } from 'astro/config'

import mdx from '@astrojs/mdx'
import compress from 'astro-compress'
import sitemap from '@astrojs/sitemap'
import fetchAndSaveFiles from './scripts/fetchFiles'

await fetchAndSaveFiles()

// https://astro.build/config
export default defineConfig({
  site: 'https://malo-polese.fr',
  integrations: [
    mdx(),
    sitemap(),
    compress({
      CSS: true,
      HTML: true,
    }),
  ],
  image: {
    domains: ['res.cloudinary.com', 'localhost', '127.0.0.1'],
  },
})
