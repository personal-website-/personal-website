import fs from 'fs';
import path from 'path';
import dotenv from 'dotenv';

dotenv.config();

const STRAPI_URL = process.env.STRAPI_URL;
const STRAPI_TOKEN = process.env.STRAPI_SCRIPTS_TOKEN;
const FILES_API = `${STRAPI_URL}/api/upload/files`;

const downloadDir = './public/files';
const filesToDownload = [
    "resume.pdf"
];

async function downloadFile(url, filename) {
    const response = await fetch(url, {
        headers: { Authorization: `Bearer ${STRAPI_TOKEN}` },
    });

    if (!response.ok) {
        console.error(`Failed to fetch ${url}: ${response.statusText}`);
        return;
    }

    const arrayBuffer = await response.arrayBuffer();
    const buffer = Buffer.from(arrayBuffer);
    const filePath = path.join(downloadDir, filename);

    fs.writeFileSync(filePath, buffer);
    console.log(`✅ Downloaded: ${filename}`);
}

export default async function fetchAndSaveFiles() {
    try {
        const response = await fetch(FILES_API, {
            headers: { Authorization: `Bearer ${STRAPI_TOKEN}` },
        });

        const files = await response.json();
        if (!fs.existsSync(downloadDir)) {
            fs.mkdirSync(downloadDir, { recursive: true });
        }

        for (const file of files) {
            if (!filesToDownload.includes(file.name)) {
                console.log(`⏭️ Skipping: ${file.name}`);
                continue;
            }
            console.log(`🆗 Downloading: ${file.name}`);
            const fileUrl = `${STRAPI_URL}${file.url}`;
            await downloadFile(fileUrl, file.name);
        }
    } catch (error) {
        console.error('Error fetching files:', error);
    }
}

